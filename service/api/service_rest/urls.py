from django.urls import path

from .views import (
    api_technicians,
    api_technician,
    api_appointments,
    api_appointment,
    api_appointment_finish,
    api_appointment_cancel,
    api_appointments_search,
)

urlpatterns = [
    path(
        "technicians/",
        api_technicians,
        name="api_technicians",
    ),
    path(
        "technicians/<int:pk>/",
        api_technician,
        name="api_technician",
    ),
    path(
        "appointments/",
        api_appointments,
        name="api_appointments",
    ),
    path(
        "appointments/<int:pk>/",
        api_appointment,
        name="api_appointment",
    ),
    path('appointments/<int:pk>/cancel/', api_appointment_cancel, name='appointment-cancel'),
    path('appointments/<int:pk>/finish/', api_appointment_finish, name='appointment-finish'),
    path('appointments/search/', api_appointments_search, name='appointments-search'),
]
