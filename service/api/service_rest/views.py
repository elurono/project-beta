from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.db.models import Q
import json

from .models import Technician, Appointment, AutomobileVO
from .encoders import TechnicianEncoder, AppointmentEncoder


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        techs_data = [
            {
                "id": tech.id,
                "first_name": tech.first_name,
                "last_name": tech.last_name,
                "employee_id": tech.employee_id,
            }
            for tech in techs
        ]
        return JsonResponse({"technicians": techs_data})
    else:
        try:
            content = json.loads(request.body)
            if Technician.objects.filter(employee_id=content.get('employee_id')).exists():
                return JsonResponse({"message": "Employee ID already exists"}, status=400)
            tech = Technician.objects.create(**content)
            return JsonResponse(
                {
                    "id": tech.id,
                    "first_name": tech.first_name,
                    "last_name": tech.last_name,
                    "employee_id": tech.employee_id,
                }
            )
        except:
            response = JsonResponse({"message": "Could not create the technician"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            tech = Technician.objects.get(pk=pk)
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            tech = Technician.objects.get(pk=pk)
            tech.delete()
            return JsonResponse({"message": "Technician successfully deleted"}, status=200)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist"}, status=404)
    else: # PUT
        try:
            content = json.loads(request.body)
            tech = Technician.objects.get(pk=pk)

            props = ["first_name", "last_name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(tech, prop, content[prop])
            tech.save()
            return JsonResponse(
                tech,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        apps = Appointment.objects.all()
        apps_dict = [AppointmentEncoder().default(app) for app in apps]
        for app in apps_dict:
            # Check if an Automobile with the same VIN exists and has been sold
            try:
                automobile = AutomobileVO.objects.filter(vin=app['vin']).first()
                if automobile and automobile.sold:
                    app['vip'] = True
                else:
                    app['vip'] = False
            except AutomobileVO.DoesNotExist:
                app['vip'] = False
        return JsonResponse({"appointments": apps_dict}, safe=False)
    else:
        try:
            content = json.loads(request.body)
            tech_id = content["technician"]
            if not Technician.objects.filter(pk=tech_id).exists():
                return JsonResponse({"message": "Technician ID does not exist"}, status=400)
            tech = Technician.objects.get(pk=tech_id)
            content["technician"] = tech
            app = Appointment.objects.create(**content)
            app_dict = AppointmentEncoder().default(app)
            return JsonResponse(app_dict, safe=False)
        except Exception as e:
            response = JsonResponse({"message": f"Could not create the appointment. Error: {str(e)}"})
            response.status_code = 400
            return response



@require_http_methods(["DELETE", "GET", "PUT"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            app = Appointment.objects.get(pk=pk)
            app_dict = AppointmentEncoder().default(app)
            # Check if an Automobile with the same VIN exists and has been sold
            try:
                automobile = AutomobileVO.objects.filter(vin=app_dict['vin']).first()
                if automobile and automobile.sold:
                    app_dict['vip'] = True
                else:
                    app_dict['vip'] = False
            except AutomobileVO.DoesNotExist:
                app_dict['vip'] = False
            return JsonResponse(app_dict, safe=False)
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            app = Appointment.objects.get(pk=pk)
            app_dict = AppointmentEncoder().default(app)
            app.delete()
            return JsonResponse(app_dict, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)
    else:  # PUT
        try:
            content = json.loads(request.body)
            app = Appointment.objects.get(pk=pk)
            props = ["date_time", "reason", "status", "vin", "customer"]
            if "technician" in content:
                tech_id = content.pop("technician")
                tech = Technician.objects.get(pk=tech_id)
                content["technician"] = tech
            for prop in props:
                if prop in content:
                    setattr(app, prop, content[prop])
            app.save()
            app_dict = AppointmentEncoder().default(app)
            return JsonResponse(app_dict, safe=False)
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def api_appointment_cancel(request, pk):
    try:
        app = Appointment.objects.get(pk=pk)
        app.status = 'canceled'
        app.save()
        return JsonResponse(
            {"message": "Appointment successfully cancelled", "status": app.status},
            status=200
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_appointment_finish(request, pk):
    try:
        app = Appointment.objects.get(pk=pk)
        app.status = 'finished'
        app.save()
        return JsonResponse(
            {"message": "Appointment successfully finished", "status": app.status},
            status=200
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Appointment does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["GET"])
def api_appointments_search(request):
    vin = request.GET.get('vin', '')
    apps = Appointment.objects.filter(Q(vin__icontains=vin)).order_by('-date_time')
    apps_dict = [AppointmentEncoder().default(app) for app in apps]
    return JsonResponse({"appointments": apps_dict}, safe=False)
