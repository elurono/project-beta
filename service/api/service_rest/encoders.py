from common.json import ModelEncoder
from .models import Technician, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = ['first_name', 'last_name', 'employee_id']  # Add any other properties you need to include


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = ['date_time', 'reason', 'status', 'vip_status', 'vin', 'customer', 'technician']
    nested_encoders = {
        'technician': TechnicianEncoder
    }
