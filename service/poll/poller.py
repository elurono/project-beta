import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import AutomobileVO

def poll(repeat=True):
    while True:
        try:
            response = requests.get('http://project-beta-inventory-api-1:8000/api/automobiles/')
            if response.status_code < 200 or response.status_code > 299:
                raise requests.HTTPError(response.status_code)

            data = response.json()

            for automobile in data.get('autos', []):
                AutomobileVO.objects.get_or_create(
                    vin=automobile['vin'],
                    defaults={'sold': automobile['sold']}
                )

        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
