from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ['first_name', 'last_name', 'employee_id']


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ['first_name', 'last_name', 'address', 'phone_number']


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['vin', 'sold']


class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ['automobile', 'salesperson', 'customer', 'price']
    nested_encoders = {
        'salesperson': SalespersonEncoder,
        'customer': CustomerEncoder,
        'automobile': AutomobileVOEncoder,
    }



@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        list_salespeople = Salesperson.objects.all()
        salespeople_info = [
            {
                "first_name": salesperson.first_name,
                "last_name": salesperson.last_name,
                "employee_id": salesperson.employee_id,
                "id": salesperson.id,
            }
            for salesperson in list_salespeople
        ]
        return JsonResponse(
            {"salespeople": salespeople_info},
        )
    else:
        try:
            content = json.loads(request.body)
            if Salesperson.objects.filter(employee_id=content.get('employee_id')).exists():
                return JsonResponse({"message": "Employee ID already assigned"}, status=400)
            create_salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                {
                    "first_name": create_salesperson.first_name,
                    "last_name": create_salesperson.last_name,
                    "employee_id": create_salesperson.employee_id,
                    "id": create_salesperson.id,
                }
            )
        except:
            response = JsonResponse({"message": "Cannot create salesperson"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(pk=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(pk=pk)
            salesperson.delete()
            return JsonResponse({"message": "Salesperson deleted"}, status=200)
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson does not exist"}, status=404)
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.get(pk=pk)

            properties = ['first_name', 'last_name', 'employee_id']
            for property in properties:
                if property in content:
                    setattr(salesperson, property, content[property])
            salesperson.save()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        list_customers = Customer.objects.all()
        customers_info = [
            {
                "first_name": customer.first_name,
                "last_name": customer.last_name,
                "address": customer.address,
                "phone_number": customer.phone_number,
                "id": customer.id,
            }
            for customer in list_customers
        ]
        return JsonResponse(
            {"customers": customers_info},
        )
    else:
        try:
            content = json.loads(request.body)
            if Customer.objects.filter(phone_number=content.get('phone_number')).exists():
                return JsonResponse({"message": "Customer already assigned"}, status=400)
            create_customer = Customer.objects.create(**content)
            return JsonResponse(
                {
                "first_name": create_customer.first_name,
                "last_name": create_customer.last_name,
                "address": create_customer.address,
                "phone_number": create_customer.phone_number,
                "id": create_customer.id,
                }
            )
        except:
            response = JsonResponse({"message": "Cannot create customer"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(pk=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(pk=pk)
            customer.delete()
            return JsonResponse({"message": "Customer deleted"}, status=200)
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"}, status=404)
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(pk=pk)

            properties = ['first_name', 'last_name', 'address', 'phone_number']
            for property in properties:
                if property in content:
                    setattr(customer, property, content[property])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        list_sales = Sale.objects.all()
        sales_dictionary = [SaleEncoder().default(sale) for sale in list_sales]
        return JsonResponse({"sales": sales_dictionary}, safe=False)
    else:
        try:
            content = json.loads(request.body)
            vin = content["vin"]
            salesperson_id = content["salesperson"]
            customer_id = content["customer"]

            if not Salesperson.objects.filter(pk=salesperson_id).exists():
                return JsonResponse({"message": "Salesperson does not exist"}, status=400)

            if not Customer.objects.filter(pk=customer_id).exists():
                return JsonResponse({"message": "Customer does not exist"}, status=400)

            if not AutomobileVO.objects.filter(pk=vin).exists():
                return JsonResponse({"message": "Automobile does not exist"}, status=400)

            get_automobile = AutomobileVO.objects.get(pk=vin)
            get_salesperson = Salesperson.objects.get(pk=salesperson_id)
            get_customer = Customer.objects.get(pk=customer_id)

            del content["vin"]
            del content["salesperson"]
            del content["customer"]

            content["automobile"] = get_automobile
            content["salesperson"] = get_salesperson
            content["customer"] = get_customer

            create_sale = Sale.objects.create(**content)
            sales = SaleEncoder().default(create_sale)

            return JsonResponse(sales, safe=False)
        except Exception as e:
            response = JsonResponse({"message": f"Cannot create sale. Error: {str(e)}"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(pk=pk)
            sale_dictionary = SaleEncoder().default(sale)
            return JsonResponse(sale_dictionary, safe=False)
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(pk=pk)
            sale_dictionary = SaleEncoder().default(sale)
            sale.delete()
            return JsonResponse(sale_dictionary, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=400)
    else:
        try:
            content = json.loads(request.body)
            sale = Sale.objects.get(pk=pk)
            properties = ["automobile", "salesperson", "customer", "price"]
            if "automobile" in content:
                auto_id = content.pop("automobile")
                get_automobile = AutomobileVO.objects.get(pk=auto_id)
                content["automobile"] = get_automobile
            for property in properties:
                if property in content:
                    setattr(sale, property, content[property])
            sale.save()
            sale_dictionary = SaleEncoder().default(sale)
            return JsonResponse(sale_dictionary, safe=False)
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
