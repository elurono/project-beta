from django.db import models
from django.urls import reverse


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.IntegerField()

    def get_api_url(self):
        return reverse("api_salesperson", kwargs={"pk": self.id})


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.TextField()
    phone_number = models.CharField(max_length=25)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    sold = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"pk": self.id})


class Sale(models.Model):
    automobile = models.ForeignKey(AutomobileVO, related_name="automobile", on_delete=models.CASCADE)
    salesperson = models.ForeignKey(Salesperson, related_name="salesperson", on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, related_name="customer", on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=12, decimal_places=2)

    def get_api_url(self):
        return reverse("api_sale", kwargs={"pk": self.id})
