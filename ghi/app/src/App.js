import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './components/TechnicianForm';
import AppointmentForm from './components/AppointmentForm';
import TechnicianList from './components/TechnicianList';
import AppointmentList from './components/AppointmentList';
import ServiceHistory from './components/ServiceHistory';
import ManufacturerList from './components/ManufacturerList';
import ManufacturerForm from './components/ManufacturerForm';
import VehicleModelList from './components/VehicleModelsList';
import VehicleModelsForm from './components/VehicleModelsForm';
import AutomobileList from './components/AutomobileList';
import AutomobileForm from './components/AutomobileForm';
import SalespersonForm from './components/SalespersonForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/create-technician" element={<TechnicianForm />} />
          <Route path="/list-technicians" element={<TechnicianList />} />
          <Route path="/create-appointment" element={<AppointmentForm />} />
          <Route path="/list-appointments" element={<AppointmentList />} />
          <Route path="/service-history" element={<ServiceHistory />} />
          <Route path="/list-manufacturers" element={<ManufacturerList />} />
          <Route path="/create-manufacturers" element={<ManufacturerForm />} />
          <Route path="/list-vehiclemodels" element={<VehicleModelList />} />
          <Route path="/create-vehiclemodels" element={<VehicleModelsForm />} />
          <Route path="/list-automobiles" element={<AutomobileList />} />
          <Route path="/create-automobiles" element={<AutomobileForm />} />
          <Route path="/create-salesperson" element={<SalespersonForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
