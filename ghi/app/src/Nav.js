import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-technician">Add a Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/list-technicians">Technicians</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-appointment">Create Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/list-appointments">Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/service-history">Service History</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/list-manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-manufacturers">Create a Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/list-vehiclemodels">Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-vehiclemodels">Create a Model</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/list-automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-automobiles">Add an Automobile to Inventory</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/create-salesperson">Add a salesperson</NavLink>
            </li>
            {/* Add other nav links here */}
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
