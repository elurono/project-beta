import React, { useState, useEffect } from 'react';

function ServiceHistory() {
  const [vin, setVin] = useState('');
  const [appointments, setAppointments] = useState([]);

  const fetchAppointments = async () => {
    let url = vin
      ? `http://localhost:8080/api/appointments/search?vin=${vin}`
      : 'http://localhost:8080/api/appointments/';

    try {
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      const localAppointments = data.appointments.map(appointment => {
        let utcDateTime = new Date(appointment.date_time);
        let localDateTime = new Date(utcDateTime.getTime() + (new Date().getTimezoneOffset() * 60000));
        let id = appointment.href.split('/').filter(Boolean).pop();

        return {
          ...appointment,
          date_time: localDateTime.toISOString(),
          id: id,
        };
      });
      setAppointments(localAppointments);
    } catch (error) {
      console.error('There was an error!', error);
      setAppointments([]);
    }
  };

  const searchAppointments = (e) => {
    e.preventDefault();
    fetchAppointments();
  };

  useEffect(() => {
    fetchAppointments();
  } );

  return (
      <div>
          <h2>Service History</h2>
          <form onSubmit={searchAppointments} style={{ marginBottom: '1rem', display: 'flex' }}>
              <input
                type="text"
                placeholder="Search by VIN..."
                value={vin}
                onChange={e => setVin(e.target.value)}
                style={{ flex: 1, marginRight: '1rem', padding: '0.5rem' }}
              />
              <input
                type="submit"
                value="Search"
                style={{ padding: '0.5rem' }}
              />
          </form>
          <table className="table" style={{ width: '100%' }}>
              <thead>
                  <tr>
                      <th>VIN</th>
                      <th>VIP</th>
                      <th>Customer</th>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Technician</th>
                      <th>Reason</th>
                      <th>Status</th>
                  </tr>
              </thead>
              <tbody>
                  {appointments.map(appointment => {
                      const dateTime = new Date(appointment.date_time);
                      return (
                          <tr key={appointment.id}>
                              <td>{appointment.vin}</td>
                              <td>{appointment.vip ? "Yes" : "No"}</td>
                              <td>{appointment.customer}</td>
                              <td>{dateTime.toLocaleDateString()}</td>
                              <td>{dateTime.toLocaleTimeString()}</td>
                              <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                              <td>{appointment.reason}</td>
                              <td>{appointment.status}</td>
                          </tr>
                      );
                  })}
              </tbody>
          </table>
      </div>
  );
}

export default ServiceHistory;
