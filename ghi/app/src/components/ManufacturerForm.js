import React, { useState } from 'react';
import '../index.css';

function ManufacturerForm() {
  const [name, setName] = useState('');


  const handleSubmit = async event => {
    event.preventDefault();
    const manufacturer = {
      name,
    };

    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(manufacturer),
      });
      const data = await response.json();
      if (response.ok) {
        alert('Manufacturer created successfully');
      } else {
        console.error('Error data:', data);
        throw new Error(`HTTP error, status = ${response.status}`);
      }
    } catch (error) {
      console.error('There has been a problem with your fetch operation:', error);
      console.error('Failed to create manufacturer:', manufacturer);
    }
  };
  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <div className="border rounded p-4">
        <h2 className="mb-4">Create a Manufacturer</h2>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="name" className="form-label">Name:</label>
            <input type="text" className="form-control" id="name" value={name} onChange={e => setName(e.target.value)} required />
          </div>
          <button type="submit" className="btn btn-primary">Create Manufacturer</button>
        </form>
      </div>
    </div>
  );
}

export default ManufacturerForm;
