import React, { useEffect, useState } from 'react';

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  const fetchAutomobiles = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      const data = await response.json();
      setAutomobiles(data.autos);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div>
      <h2>Automobile List</h2>
      <table className="table">
        <thead>
          <tr>
            <th>Color</th>
            <th>Year</th>
            <th>VIN</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(auto => (
            <tr key={auto.id}>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.vin}</td>
              <td>{auto.model.name}</td>
              <td>{auto.model.manufacturer.name}</td>
              <td>{auto.sold ? 'Yes' : 'No'}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;
