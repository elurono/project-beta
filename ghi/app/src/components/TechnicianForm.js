import React, { useState } from 'react';

function TechnicianForm() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:8080/api/technicians/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          first_name: firstName,
          last_name: lastName,
          employee_id: employeeId,
        }),
      });

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      await response.json();

      setFirstName('');
      setLastName('');
      setEmployeeId('');
      alert('Technician created successfully!');
      await fetchTechnicians();
    } catch (error) {
      console.error('Error:', error);
      alert('Failed to create the technician.');
    }
  };

  async function fetchTechnicians() {
    try {
      const response = await fetch('http://localhost:8080/api/technicians/');
      const data = await response.json();

      console.log(data);
    } catch (error) {
      console.error('Error:', error);
    }
  }

  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <div className="border rounded p-4">
        <h2 className="mb-4">Add a Technician</h2>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="firstName" className="form-label">First Name:</label>
            <input type="text" className="form-control" id="firstName" value={firstName} onChange={e => setFirstName(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="lastName" className="form-label">Last Name:</label>
            <input type="text" className="form-control" id="lastName" value={lastName} onChange={e => setLastName(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="employeeId" className="form-label">Employee ID:</label>
            <input type="text" className="form-control" id="employeeId" value={employeeId} onChange={e => setEmployeeId(e.target.value)} required />
          </div>
          <button type="submit" className="btn btn-primary">Add Technician</button>
        </form>
      </div>
    </div>
  );
}

export default TechnicianForm;
