import React, { useState, useEffect } from 'react';
import '../index.css';

function AutomobileForm() {
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVin] = useState('');
  const [modelId, setModelId] = useState('');
  const [models, setModels] = useState([]);

  const handleSubmit = async event => {
    event.preventDefault();
    const automobile = {
      color,
      year,
      vin,
      model_id: modelId,
    };

    try {
      const response = await fetch('http://localhost:8100/api/automobiles/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(automobile),
      });
      const data = await response.json();
      if (response.ok) {
        alert('Automobile created successfully');
      } else {
        console.error('Error data:', data);
        throw new Error(`HTTP error, status = ${response.status}`);
      }
    } catch (error) {
      console.error('There has been a problem with your fetch operation:', error);
      console.error('Failed to create automobile:', automobile);
    }
  };

  useEffect(() => {
    fetchModels();
  }, []);

  const fetchModels = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/models/');
      const data = await response.json();
      setModels(data.models);
    } catch (error) {
      console.error('Error:', error);
    }
  }

  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <div className="border rounded p-4">
        <h2 className="mb-4">Create an Automobile</h2>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="vin" className="form-label">VIN:</label>
            <input type="text" className="form-control" id="vin" value={vin} onChange={e => setVin(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="color" className="form-label">Color:</label>
            <input type="text" className="form-control" id="color" value={color} onChange={e => setColor(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="year" className="form-label">Year:</label>
            <input type="number" className="form-control" id="year" value={year} onChange={e => setYear(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="model" className="form-label">Model:</label>
            <select id="model" className="form-control" value={modelId} onChange={e => setModelId(e.target.value)} required>
              <option value="">Select a model...</option>
              {models.map(model => (
                <option key={model.id} value={model.id}>{model.name}</option>
              ))}
            </select>
          </div>
          <button type="submit" className="btn btn-primary">Create Automobile</button>
        </form>
      </div>
    </div>
  );
}

export default AutomobileForm;
