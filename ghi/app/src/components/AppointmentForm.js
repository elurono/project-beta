import React, { useState, useEffect } from 'react';
import '../index.css';

function AppointmentForm() {
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [technician, setTechnician] = useState('');
  const [reason, setReason] = useState('');
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    fetchTechnicians();
  }, []);

  const fetchTechnicians = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/technicians/');
      const data = await response.json();
      setTechnicians(data.technicians);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const handleSubmit = async event => {
    event.preventDefault();
    const localDateTime = new Date(`${date}T${time}`);
    let utcDateTime = new Date(Date.UTC(localDateTime.getFullYear(), localDateTime.getMonth(), localDateTime.getDate(), localDateTime.getHours(), localDateTime.getMinutes(), localDateTime.getSeconds()));
    const appointment = {
      vin,
      customer,
      date_time: utcDateTime.toISOString(),
      technician: parseInt(technician, 10),
      reason,
      status: 'Scheduled',
    };
    try {
      const response = await fetch('http://localhost:8080/api/appointments/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(appointment),
      })
      if (response.ok) {
        alert('Appointment created successfully');
      } else {
        console.error('Failed to create appointment:', appointment);
        throw new Error(`HTTP error, status = ${response.status}`);
      }
    } catch (error) {
      console.error('There has been a problem with your fetch operation:', error);
    }
  };

  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <div className="border rounded p-4">
        <h2 className="mb-4">Create a Service Appointment</h2>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="vin" className="form-label">VIN:</label>
            <input type="text" className="form-control" id="vin" value={vin} onChange={e => setVin(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="customer" className="form-label">Customer:</label>
            <input type="text" className="form-control" id="customer" value={customer} onChange={e => setCustomer(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="date" className="form-label">Date:</label>
            <input type="date" className="form-control" id="date" value={date} onChange={e => setDate(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="time" className="form-label">Time:</label>
            <input type="time" className="form-control" id="time" value={time} onChange={e => setTime(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="technician" className="form-label">Technician:</label>
            <select
              id="technician"
              className="form-control"
              value={technician}
              onChange={e => setTechnician(e.target.value)}
              required
            >
              <option value="">--Please choose a technician--</option>
              {technicians.map(tech => (
                <option key={tech.id} value={tech.id}>
                  {tech.first_name} {tech.last_name}
                </option>
              ))}
            </select>
          </div>
          <div className="mb-3">
            <label htmlFor="reason" className="form-label">Reason:</label>
            <textarea className="form-control" id="reason" value={reason} onChange={e => setReason(e.target.value)} required></textarea>
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  );
}

export default AppointmentForm;
