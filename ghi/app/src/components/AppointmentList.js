import React, { useEffect, useState } from 'react';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    fetchAppointments();
  }, []);

  const fetchAppointments = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/appointments/');
      const data = await response.json();
      const localAppointments = data.appointments.map(appointment => {
        let utcDateTime = new Date(appointment.date_time);
        let localDateTime = new Date(utcDateTime.getTime() + (new Date().getTimezoneOffset() * 60000));
        let id = appointment.href.split('/').filter(Boolean).pop();

        return {
          ...appointment,
          date_time: localDateTime.toISOString(),
          id: id,
        };
      });
      setAppointments(localAppointments);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const cancelAppointment = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
        method: 'PUT',
      })
      if (response.ok) {
        fetchAppointments();
      } else {
        console.error('Error:', response);
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  const finishAppointment = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
        method: 'PUT',
      })
      if (response.ok) {
        fetchAppointments();
      } else {
        console.error('Error:', response);
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div>
      <h2>Appointment List</h2>
      <table className="table">
        <thead>
          <tr>
            <th>Customer</th>
            <th>VIN</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Is VIP?</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            const dateTime = new Date(appointment.date_time);
            return (
              <tr key={appointment.id}>
                <td>{appointment.customer}</td>
                <td>{appointment.vin}</td>
                <td>{dateTime.toLocaleDateString()}</td>
                <td>{dateTime.toLocaleTimeString()}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.vip ? "Yes" : "No"}</td>
                <td>
                  <button onClick={() => cancelAppointment(appointment.id)} style={{color: 'white', backgroundColor: 'red'}}>Cancel</button>
                  <button onClick={() => finishAppointment(appointment.id)} style={{color: 'white', backgroundColor: 'green'}}>Finish</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
