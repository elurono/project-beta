import React, { useEffect, useState } from 'react';

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    fetchManufacturers();
  }, []);


  const fetchManufacturers = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/');
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } catch (error) {
      console.error('Error:', error);
    }
  };


  return (
    <div>
      <h2>Manufacturer List</h2>
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => (
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturerList;
