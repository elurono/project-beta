import React, { useState, useEffect } from 'react';

function VehicleModelsForm() {
  const [name, setName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturerId, setManufacturerId] = useState('');
  const [manufacturers, setManufacturers] = useState([]);

  const fetchManufacturers = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/');
      const data = await response.json();
      setManufacturers(data.manufacturers);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  const handleSubmit = async event => {
    event.preventDefault();
    const model = {
      name,
      picture_url: pictureUrl,
      manufacturer_id: manufacturerId,
    };

    try {
      const response = await fetch('http://localhost:8100/api/models/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(model),
      });
      const data = await response.json();
      if (!response.ok) {
        console.error('Error data:', data);
        throw new Error(`HTTP error, status = ${response.status}`);
      } else {
        alert('Model created successfully');
      }
    } catch (error) {
      console.error('There has been a problem with your fetch operation:', error);
      console.error('Failed to create model:', model);
    }
  };

  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <div className="border rounded p-4">
        <h2 className="mb-4">Create a Vehicle Model</h2>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="name" className="form-label">Name:</label>
            <input type="text" className="form-control" id="name" value={name} onChange={e => setName(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="pictureUrl" className="form-label">Picture URL:</label>
            <input type="text" className="form-control" id="pictureUrl" value={pictureUrl} onChange={e => setPictureUrl(e.target.value)} required />
          </div>
          <div className="mb-3">
            <label htmlFor="manufacturerId" className="form-label">Manufacturer:</label>
            <select className="form-control" id="manufacturerId" value={manufacturerId} onChange={e => setManufacturerId(e.target.value)} required>
              <option value="">Select a manufacturer</option>
              {manufacturers.map(manufacturer => (
                <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
              ))}
            </select>
          </div>
          <button type="submit" className="btn btn-primary">Create Model</button>
        </form>
      </div>
    </div>
  );
}

export default VehicleModelsForm;
