import React, { useEffect, useState } from 'react';

function VehicleModelList() {
  const [vehicleModels, setVehicleModels] = useState([]);

  const fetchVehicleModels = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/models/');
      const data = await response.json();
      setVehicleModels(data.models);
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(() => {
    fetchVehicleModels();
  }, []);

  return (
    <div>
      <h2>Models</h2>
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>
          {vehicleModels.map(model => (
            <tr key={model.id}>
              <td>{model.name}</td>
              <td>{model.manufacturer.name}</td>
              <td><img src={model.picture_url} alt={model.name} /></td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default VehicleModelList;
