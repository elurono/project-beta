# CarCar

Team:

* Eluron Osiebo - Service
* Ade Adesanoye - Sales

## Design

## Service microservice

Technician model: Represents a mechanic with a unique employee ID and name.

AutomobileVO model: This represents a car in the inventory. VIN, and a sold status.

Appointment model: Represents a booking for a car to be serviced. It contains a date/time, reason for the appointment, status, VIN of the car to be serviced, the customer's name, and the assigned technician.

When appointments are fetched or created, the microservice checks the AutomobileVO model (which represents the inventory microservice) to mark the appointment as VIP if the car associated with the appointment (doing so by the VIN) is sold. Also, it makes sure that the technician assigned to an appointment exists before creating it.

## Sales microservice

Salesperson model - Represents a salesperson that is identified with a unique name and employee id.

AutomobileVO model - Represents the car in the inventory, its VIN, and whether the vehicle is sold

Customer model - Represents a customer by first and last name, address, and phone number

Sales model - Represents a sale and contains the car from the inventory, the customer that is purchasing, the salesperson that sold it, and the price of the vehicle

When a Sale is inquired the AutomobilVO model allows us to see if the car is in the inventory. If the car is available it can be sold by salespeople to customers for the price given. Each vehicle can only be sold once since they all have unique identifiers (VIN). Once they are sold they are dded to a list of Sold vehicles that document who sold it, who they sold it too, and for how much.
